= Writing Gradle Plugins in Groovy : Creating the Skeleton Plugin
Schalk W. Cronjé <ysb33r@gmail.com>

== Maven coordinates

Plugins are JAR artifacts and as they will need to be published to useful to others they need to have Maven coordinates. Choose your Maven group and version and add it to the `build.gradle` file.

.build.gradle
[source,groovy]
----
include::{exercisesdir}/build.gradle[tags=specify-group-and-version]
----
<1> Maven group is set by assigning to the global `group` property.
<2> Version is set by assigning to the global `version` property.

NOTE: The maven artifact name will be taken from the project name. If this is a single project it will be the `rootProject.name` as specified in `settings.gradle`. For multi-projects it will be the name of the subproject where the plugin is built. It is important to choose the directory name carefully or explicitly set a name within `settings.gradle`.

== Set JDK compatibility level

Unless the plugin requires a specific lvel of functionality in the JDK or are dependent on libraries that require a higher level compatibility, the level always has to be set to the minimum level that the the current version of Gradle requires to be run.

.build.gradle
[source,groovy]
----
include::{exercisesdir}/build.gradle[tags=jdk-compatibility]
----

== Plugin identifier

Choose an identifier for your plugin. This must follow a domain-like namespace and should in most circumstances exclude `gradle` as a word. It could follow your package naming convention, but it does not have to.

(The rest of this workshop will use `com.example.pluginworkshop`).

== Start by writing a test

In many cases it is best to add a test just to know that your plugin will be applied. Add a test that in this case will just check for a simple extension that is being created. (You can always remove it later, once you are more comfortable with the process).

.src/test/groovy/pluginworkshop/WorkshopPluginSpec.groovy
[source,groovy]
----
include::{unittestdir}/WorkshopPluginSpec.groovy[tags=first-test]
----
<1> When a plugin is tested a Gradle {gradle-api}/org/gradle/api/Project.html[Project] object is instantiated by using a {gradle-api}/org/gradle/testfixtures/ProjectBuilder.html[ProjectBuilder] instance.
<2> Using `project.allprojects` allows for writing code in a closure that closely matches that which a use will write in a build script.
<3> In unit tests usage of `apply plugin` is requiresd as `plugins {}` functionality is not available.

Now run your build.

[listing]
----
./gradlew test

:compileJava NO-SOURCE
:compileGroovy NO-SOURCE
:processResources NO-SOURCE
:classes UP-TO-DATE
:compileTestJava NO-SOURCE
:compileTestGroovy
:processTestResources NO-SOURCE
:testClasses
:test

pluginworkshop.WorkshopPluginSpec > Plugin must load when applied FAILED
    org.gradle.api.plugins.UnknownPluginException at WorkshopPluginSpec.groovy:16

1 test completed, 1 failed
:test FAILED
----

It has failed with `UnknownPluginException` as it could not find a match for the identifier in apply plugin. This is done in the `META-INF/gradle-plugins` folder as a file named with the plugin identifier + `.properties`. If the plugin identifier is `com.example.pluginworkshop`, the the file will be named `com.example.pluginworkshop.properties`.

The properties file contains a single line starting with `implementation-class=` and having the name of the plugin class appended.

Proceed and add the identifier.

.src/main/resources/META-INF/gradle-plugins/com.example.pluginworkshop.properties
[listing]
----
include::{resourcedir}/com.example.pluginworkshop.properties[]
----

Now run the test again.

[listing]
----
./gradlew test

:compileJava NO-SOURCE
:compileGroovy NO-SOURCE
:processResources
:classes
:compileTestJava NO-SOURCE
:compileTestGroovy
:processTestResources NO-SOURCE
:testClasses
:test

pluginworkshop.WorkshopPluginSpec > Plugin must load when applied FAILED
    org.gradle.api.plugins.InvalidPluginException at WorkshopPluginSpec.groovy:16
        Caused by: java.lang.ClassNotFoundException at WorkshopPluginSpec.groovy:16

1 test completed, 1 failed
:test FAILED
----

If your plugin identifier in your uni test matched the name of the properties file, your test will have failed with a `ClassNotFoundException`.

Proceed to add the plugin class

.src/main/groovy/pluginworkshop/WorkshopPlugin.groovy
[source,groovy]
----
include::{sourcedir}/WorkshopPlugin.groovy[tags=first-layout]
----
<1> Prefer `CompileStatic` for performance reasons and stable APIs across Gradle versions.
<2> A class that is applied via `apply plugin`, must implement the {gradle-api}/org/gradle/api/Plugin.html[Plugin<Project>] interface.

Run the test again.

[listing]
----
./gradlew test

:compileJava NO-SOURCE
:compileGroovy
:processResources UP-TO-DATE
:classes
:compileTestJava NO-SOURCE
:compileTestGroovy
:processTestResources NO-SOURCE
:testClasses
:test

pluginworkshop.WorkshopPluginSpec > Plugin must load when applied FAILED
    groovy.lang.MissingPropertyException at WorkshopPluginSpec.groovy:20

1 test completed, 1 failed
:test FAILED
----

Open the report in a browser and inspect it

image::first-plugin-failure-report.png[]

It should have failed due to the missing `workshop` property. Proceed now to add the extension and run the test again, which should pass.

.src/main/groovy/pluginworkshop/WorkshopPlugin.groovy
[source,groovy]
----
include::{sourcedir}/WorkshopPlugin.groovy[tags=with-simple-extension,indent=0]
----
<1> Due to static compilation `project.ext` will fail. It is better to use {gradle-api}/org/gradle/api/plugins/ExtraPropertiesExtension.html[project.extensions.extraProperties] instead.

'''

*Next*: link:creating-a-task-type.html[Creating a Task Type]