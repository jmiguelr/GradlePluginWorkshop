// tag::task-skeleton[]
package pluginworkshop

import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.InputFiles

// end::task-skeleton[]


// tag::task-skeleton[]

@CompileStatic
class FileManifest extends DefaultTask { // <1>
    // end::task-skeleton[]

    // tag::manifest-files-detail[]
    private List<Object> manifestFiles = [] // <1>

    // tag::manifest-files[]
    void setManifestFiles( final List<Object> replaceWith ) {
        // end::manifest-files[]
        this.manifestFiles.clear() // <2>
        this.manifestFiles.addAll( replaceWith )
        // tag::manifest-files[]
    }

    void manifestFiles( Object... newFiles ) {
        // end::manifest-files[]
        this.manifestFiles.addAll( newFiles as List ) // <3>
        // tag::manifest-files[]
    }

    @InputFiles
    FileCollection getManifestFiles() {
        // end::manifest-files[]
        project.files(this.manifestFiles) // <4>
        // tag::manifest-files[]
    }
    // end::manifest-files[]
    // end::manifest-files-detail[]

    // tag::output-file[]
    private Object outputFile = { "${project.buildDir}/workshop-manifest.txt"}// <1>

    void setOutputFile(final Object dest) {
        this.outputFile = dest // <2>
    }

    @OutputFile // <3>
    File getOutputFile() {
        project.file(this.outputFile) // <4>
    }
    // end::output-file[]

    // tag::task-skeleton[]
    @TaskAction  // <2>
    // tag::action-detail[]
    void exec() {
        // end::task-skeleton[]
        File target = getOutputFile()
        FileCollection fc = getManifestFiles() // <1>
        target.withWriter { w ->
            for( File input : fc.files ) { // <2>
                w << input.absolutePath << '  '
                w.println input.size()
            }
        }
        // tag::task-skeleton[]
    }
    // end::action-detail[]
    // end::task-skeleton[]

// tag::task-skeleton[]
}
// end::task-skeleton[]

