// tag::initial-test[]
package pluginworkshop

import org.gradle.api.Project
import org.gradle.api.file.FileCollection
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class FileManifestSpec extends Specification {

    Project project = ProjectBuilder.builder().build()

    def 'FileManifest must accept a variety of input types'() {

        when: 'A manifest task is configured with strings, files & closures'
        project.tasks.create( 'manifest', FileManifest ) { // <1> <2>
            manifestFiles 'a.txt' // <3>
            manifestFiles new File('b.txt'), project.file('c.txt') // <4> <5> <6>
            manifestFiles { 'd.txt' } // <7>
        }
        Set<File> files = project.manifest.manifestFiles.files // <8>

        then: 'They must all be converted to members of a FileCollection'
        files.contains( new File(project.projectDir,'a.txt') )
        files.contains( new File(project.projectDir,'b.txt') )
        files.contains( new File(project.projectDir,'c.txt') )
        files.contains( new File(project.projectDir,'d.txt') )
    }
// end::initial-test[]

    // tag::output-file-test[]
    def 'FileManifest has a single output file'() {

        when: 'The manifest task is created with no output assignment'
        FileManifest task = project.tasks.create( 'manifest', FileManifest ) // <1>

        then: 'A default value will be used'
        task.outputFile == new File(project.buildDir,'workshop-manifest.txt')

        when: 'The manifest task is created with output assignment'
        task = project.tasks.create( 'manifest2', FileManifest )
        task.outputFile { "${project.buildDir}/my-manifest.txt"} // <2> <3>

        then: 'The assigned value must be used'
        task.outputFile == new File(project.buildDir,'my-manifest.txt')
    }
    // end::output-file-test[]

    // tag::create-manifest[]
    def 'FileManifest must list each file with absolute path and size'() {

        setup: 'Adding four files to project'
        def inputFiles = (1..4).collect {
            File f = new File(project.projectDir,"${it}.txt")
            f.text = 'a'.multiply(it)
            return f
        }

        when: 'The task is created'
        FileManifest task = project.tasks.create('manifest',FileManifest)
        task.manifestFiles inputFiles

        and: 'The task is executed'
        task.execute() // <1>
        File outputFile = task.outputFile // <2>

        then: 'The output file must exist'
        outputFile.exists()

        when: 'the content of manifest file is inspected'
        def lines = outputFile.readLines()

        then: 'The manifest file should contain four entries'
        lines.size() == 4

        and: 'Each input file should be listed with the correct size'
        lines.collect { line ->
            File target = inputFiles.find { file ->
                file.absolutePath.startsWith(line)
            }
            if(target == null) {
                return false
            }
            target.size() == line.replace(target.absolutePath,'').toLong()
        }.every { true }
    }
    // end::create-manifest[]

// tag::initial-test[]
}
// end::initial-test[]
